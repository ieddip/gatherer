(ns gatherer.core
  (:require [clojure.string :as string]
            [clojure.edn :as edn]))

;;;; HACK: Write tests. (mae 15-10-23)

(defn gather-plugin-metas []
  (enumeration-seq (.. Thread
                       currentThread
                       getContextClassLoader
                       (getResources "plugin.edn"))))

(defn read-plugin-meta [url] (edn/read-string (slurp url)))

(defn meta->exports [m] (get m :exports))

(defn require-export [& args]
  (map (fn [c]
         (let [s (-> c str symbol)] (require s) s))
       args))

(defn require-plugins
  "Requires all exported namespaces found in plugin.edn files which
  are on the classpath."
  []
  (map (comp (partial apply require-export) meta->exports read-plugin-meta)
       (gather-plugin-metas)))
