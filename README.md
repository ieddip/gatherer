# gatherer

> requires all exported graphite components found on the classpath

Searches all plugin.edn files on the classpath and `requires` exported
libraries.

### Most Recent Release

With Leiningen:

```
[gatherer "1.0-snapshot"]
```

With Maven:

```
<dependency>
  <artifactId>gatherer</artifactId>
  <version>1.0-snapshot</version>
</dependency>
```


## Quick-start

```clojure
(use 'gatherer.core)

(require-plugins)
;;=> list of required components
```
## Building and deployment

Define build repositories as local profiles, whether project (create profiles.clj in project folder), 
user (add profiles to ~/.lein/profiles.clj) or system (add profiles to /etc/leiningen/profiles.clj) specific.

```clojure

;;;; PROFILE EXAMPLE

{:graphite-debug {:repositories [["snapshots" {:url "<url-of-snapshot-repository>"
                                               :username "<repo-user>" 
                                               :password "<repo-password>"}]]}

 :graphite-release {:repositories [["internal" {:url "<url-of-release-repository>"
                                                :username "<repo-user>" 
                                                :password "<repo-password>"}]]}}
```
Then build with e.g.

```bash
lein with-profile graphite-debug deploy snapshots
```

## License

Copyright © 2015 IVGI, FHNW

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
